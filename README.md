## Mobile Developer Test

### Library Use

* Butterknife  - this is for easily handling the views from activity
* Retrofit - this is use for handling REST API request from server
* Picasso - this is use for handling iamges cache
* Databinding - this is use for handling screenorientation change for retaining current state of activity

##### Tools

* Android Studio
* PojoGson converter

##### Implementation

* Using MVP as main design architecture
* Databinding for all datas receiving from server for easily transfer data to views
