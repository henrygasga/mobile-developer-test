package com.developer.mobiledevelopertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {
    @SerializedName("price_conditions")
    @Expose
    private String priceConditions;

    public String getPriceConditions() {
        return priceConditions;
    }

    public void setPriceConditions(String priceConditions) {
        this.priceConditions = priceConditions;
    }
}
