package com.developer.mobiledevelopertest.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MetaDataModel {
    @SerializedName("product_count")
    @Expose
    private String productCount;
    @SerializedName("results")
    @Expose
    private JsonElement results = null;

    public String getProductCount() {
        return productCount;
    }

    public void setProductCount(String productCount) {
        this.productCount = productCount;
    }

    public JsonElement getResults() {
        return results;
    }

    public void setResults(JsonElement results) {
        this.results = results;
    }
}
