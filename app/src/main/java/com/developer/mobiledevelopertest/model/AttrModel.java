package com.developer.mobiledevelopertest.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttrModel {
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("year_built")
    @Expose
    private String yearBuilt;
    @SerializedName("engine")
    @Expose
    private String engine;
    @SerializedName("price_conditions")
    @Expose
    private String priceConditions;
    @SerializedName("price_conditions_id")
    @Expose
    private String priceConditionsId;
    @SerializedName("color_family")
    @Expose
    private String colorFamily;
    @SerializedName("seats")
    @Expose
    private String seats;
    @SerializedName("doors")
    @Expose
    private String doors;
    @SerializedName("drive_type")
    @Expose
    private String driveType;
    @SerializedName("warranty_type")
    @Expose
    private String warrantyType;
    @SerializedName("warranty_years")
    @Expose
    private String warrantyYears;
    @SerializedName("warranty_kms")
    @Expose
    private String warrantyKms;
    @SerializedName("all")
    @Expose
    private AllModel all;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(String yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getPriceConditions() {
        return priceConditions;
    }

    public void setPriceConditions(String priceConditions) {
        this.priceConditions = priceConditions;
    }

    public String getPriceConditionsId() {
        return priceConditionsId;
    }

    public void setPriceConditionsId(String priceConditionsId) {
        this.priceConditionsId = priceConditionsId;
    }

    public String getColorFamily() {
        return colorFamily;
    }

    public void setColorFamily(String colorFamily) {
        this.colorFamily = colorFamily;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(String warrantyType) {
        this.warrantyType = warrantyType;
    }

    public String getWarrantyYears() {
        return warrantyYears;
    }

    public void setWarrantyYears(String warrantyYears) {
        this.warrantyYears = warrantyYears;
    }

    public String getWarrantyKms() {
        return warrantyKms;
    }

    public void setWarrantyKms(String warrantyKms) {
        this.warrantyKms = warrantyKms;
    }

    public AllModel getAll() {
        return all;
    }

    public void setAll(AllModel all) {
        this.all = all;
    }
}
