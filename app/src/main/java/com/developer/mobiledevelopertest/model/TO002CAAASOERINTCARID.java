package com.developer.mobiledevelopertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TO002CAAASOERINTCARID {
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("attributes")
    @Expose
    private Attributes attributes;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }
}
