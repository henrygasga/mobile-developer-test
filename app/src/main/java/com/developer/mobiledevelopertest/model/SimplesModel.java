package com.developer.mobiledevelopertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SimplesModel {
    @SerializedName("TO002CAAASOERINTCARID")
    @Expose
    private TO002CAAASOERINTCARID tO002CAAASOERINTCARID;

    public TO002CAAASOERINTCARID getTO002CAAASOERINTCARID() {
        return tO002CAAASOERINTCARID;
    }

    public void setTO002CAAASOERINTCARID(TO002CAAASOERINTCARID tO002CAAASOERINTCARID) {
        this.tO002CAAASOERINTCARID = tO002CAAASOERINTCARID;
    }
}
