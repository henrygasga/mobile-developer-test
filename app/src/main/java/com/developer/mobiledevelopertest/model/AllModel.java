package com.developer.mobiledevelopertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllModel {
    @SerializedName("details")
    @Expose
    private List<DetailsModel> details = null;
    @SerializedName("optional")
    @Expose
    private List<OptionalModel> optional = null;

    public List<DetailsModel> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsModel> details) {
        this.details = details;
    }

    public List<OptionalModel> getOptional() {
        return optional;
    }

    public void setOptional(List<OptionalModel> optional) {
        this.optional = optional;
    }
}
