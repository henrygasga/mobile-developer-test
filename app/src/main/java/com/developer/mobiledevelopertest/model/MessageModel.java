package com.developer.mobiledevelopertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageModel {
    @SerializedName("success")
    @Expose
    private List<String> success = null;

    public List<String> getSuccess() {
        return success;
    }

    public void setSuccess(List<String> success) {
        this.success = success;
    }
}
