package com.developer.mobiledevelopertest.api;

import com.developer.mobiledevelopertest.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/api/cars/page:{page}/maxitems:{maxitems}/sort:{sort}/")
    Call<ResponseModel> getProducts(@Path("page") int page, @Path("maxitems") int maxitems,@Path("sort") String sort);

}
