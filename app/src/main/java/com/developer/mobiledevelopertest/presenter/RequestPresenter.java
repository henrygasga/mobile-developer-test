package com.developer.mobiledevelopertest.presenter;

import android.content.Context;
import android.util.Log;

import com.developer.mobiledevelopertest.Constant;
import com.developer.mobiledevelopertest.api.ApiService;
import com.developer.mobiledevelopertest.model.DataModel;
import com.developer.mobiledevelopertest.model.MetaDataModel;
import com.developer.mobiledevelopertest.model.ResponseModel;
import com.developer.mobiledevelopertest.model.ResultsModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestPresenter implements RequestContract.Presenter {

    private static Retrofit retrofit = null;
    private RequestContract.View view;

    public RequestPresenter(RequestContract.View view) {
        this.view = view;
    }

    //handling request data from api server
    @Override
    public void requestData(int page, int maxitems, String sort, Context context) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiService apiService = retrofit.create(ApiService.class);

        Call<ResponseModel> call = apiService.getProducts(page, maxitems, sort);

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                MetaDataModel metaDataModel = response.body().getMetadata();
                Type listType = new TypeToken<ArrayList<ResultsModel>>() {}.getType();
                ArrayList<ResultsModel> items = new Gson().fromJson(response.body().getMetadata().getResults(), listType);

                Log.d("RequestPresenter", "Number of request received: " + metaDataModel.getProductCount());
                view.onRequestSuccess(items);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable throwable) {
                Log.e("RequestPresenter", throwable.toString());
            }
        });
    }
}
