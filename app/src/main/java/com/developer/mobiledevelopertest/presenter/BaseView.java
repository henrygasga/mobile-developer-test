package com.developer.mobiledevelopertest.presenter;

import android.content.Context;

public interface BaseView {
    Context getContext();
    void showProgress();
    void hideProgress();
    void showError(String message);
}
