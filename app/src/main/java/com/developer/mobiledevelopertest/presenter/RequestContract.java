package com.developer.mobiledevelopertest.presenter;

import android.content.Context;

import com.developer.mobiledevelopertest.model.ResultsModel;

import java.util.List;

//setup the views and presenter
public interface RequestContract {
    interface View extends BaseView {
        void onRequestSuccess(List<ResultsModel> items);
    }
    interface Presenter {
        void requestData(
                int page,
                int maxitems,
                String sort,
                Context context
        );
    }
}
