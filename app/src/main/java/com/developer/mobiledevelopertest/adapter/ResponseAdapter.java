package com.developer.mobiledevelopertest.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.mobiledevelopertest.model.DataModel;

import java.util.ArrayList;
import java.util.List;

public class ResponseAdapter<T> extends RecyclerView.Adapter<ResponseAdapter.BindingHolder>{

    private List<T> items;
    private int layoutId;
    private int brId;
    private Context context;

    public class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;
        T item;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
//            imageview_child_item = (ImageView) v.findViewById(R.id.imageview_child_item);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public ResponseAdapter(List<T> items, int layoutId, int brId, Context context) {
        this.items = new ArrayList<>(items);
        this.layoutId = layoutId;
        this.brId = brId;
        this.context = context;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        BindingHolder holder = new BindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ResponseAdapter.BindingHolder holder, int position) {
        T item = items.get(position);
        holder.getBinding().setVariable(brId, item);

        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notifyDataSetChanged(List<T> item) {
        this.items = item;
        notifyDataSetChanged();
    }
}
