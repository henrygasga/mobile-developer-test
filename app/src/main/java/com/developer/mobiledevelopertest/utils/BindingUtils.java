package com.developer.mobiledevelopertest.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.developer.mobiledevelopertest.R;
import com.squareup.picasso.Picasso;

//this class is use for handling image cache for easily load
public class BindingUtils {
    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String url) {
        if (url == null || url.isEmpty() || url.equalsIgnoreCase("")) {
            Picasso.with(view.getContext())
                    .load(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .fit()
                    .into(view);
        } else {
//            url = (url.contains("http://") || url.contains("https://")) ? url : Const.USER_FILES_URL + url;
            Picasso.with(view.getContext())
                    .load(url)
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .fit()
                    .into(view);
        }
    }
}
