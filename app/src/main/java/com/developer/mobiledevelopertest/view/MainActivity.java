package com.developer.mobiledevelopertest.view;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.developer.mobiledevelopertest.BR;
import com.developer.mobiledevelopertest.R;
import com.developer.mobiledevelopertest.adapter.ResponseAdapter;
import com.developer.mobiledevelopertest.model.DataModel;
import com.developer.mobiledevelopertest.model.ResultsModel;
import com.developer.mobiledevelopertest.presenter.RequestContract;
import com.developer.mobiledevelopertest.presenter.RequestPresenter;
import com.developer.mobiledevelopertest.utils.EndlessRecyclerViewScrollListener;
import com.endlessrecyclerview.android.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements RequestContract.View, SwipeRefreshLayout.OnRefreshListener {

    protected RequestContract.Presenter presenter;
    private List<ResultsModel> itemsList = null;
    private ResponseAdapter<ResultsModel> adapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    String sort = "oldest";
    boolean onScroll = false;

    @BindView(R.id.rv_list) RecyclerView rvList;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.my_toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        //setting presenter for api request
        presenter = new RequestPresenter(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(layoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());

        //setup swiperefresh
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
                requestDataFromServer();
            }
        });

        itemsList = new ArrayList<>();

        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                onScroll = true;
                loadNextPage(page);
            }
        };

        rvList.addOnScrollListener(scrollListener);

        adapter = new ResponseAdapter<>(itemsList,
                R.layout.item_response,
                BR.model,
                getContext());
        rvList.setAdapter(adapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_mh:
                sort = "mileage-high";
                requestDataFromServer();
                return true;
            case R.id.action_ml:
                sort = "mileage-low";
                requestDataFromServer();
                return true;
            case R.id.action_newest:
                sort = "newest";
                requestDataFromServer();
                return true;
            case R.id.action_oldest:
                sort = "oldest";
                requestDataFromServer();
                return true;
            case R.id.action_ph:
                sort = "price-high";
                requestDataFromServer();
                return true;
            case R.id.action_pl:
                sort = "price-low";
                requestDataFromServer();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    //request data from server using presenter
    public void requestDataFromServer() {

        if (itemsList != null) {
            itemsList.clear();
        }
        adapter.notifyDataSetChanged(itemsList);

        presenter.requestData(1,15,sort, getContext());
    }

    //getting next page on scroll
    public void loadNextPage(int page) {
        Toast.makeText(MainActivity.this, "Getting Data....", Toast.LENGTH_SHORT).show();
        presenter.requestData(page,15,sort, getContext());
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {
        swipeRefreshLayout.setRefreshing(false);
    }


    //handling success request from server
    @Override
    public void onRequestSuccess(List<ResultsModel> items) {
        if(onScroll) {
            onScroll = false;
            itemsList.addAll(items);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyItemRangeInserted(adapter.getItemCount(), itemsList.size() - 1);
                }
            }, 1000);

        } else {
            if (itemsList != null) {
                itemsList.clear();
            }

            itemsList = items;

            adapter.notifyDataSetChanged(items);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        requestDataFromServer();
    }

}
